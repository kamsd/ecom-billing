<?php

namespace Billing;

/* Юридические лица, от имени которых создаются документы */

class Business
{

    /* Constructor */
	
	public function __construct()
	{
        $this->db = new \Billing\DB(); // подключение к БД
    }
	
	/* Получение списка */
	public function all($inn = NULL) 
	{
        
        $business = array();

        if ($inn === NULL) {
            $select = $this->db->query('SELECT * FROM business ORDER BY id DESC');
        } 
        else {
            $select = $this->db->query('SELECT * FROM business WHERE inn = ? LIMIT 1', $inn);
        }

        $all = $select->fetchAll();

        foreach ($all as $val) {
            $val['info'] = json_decode($val['info'], true);
            $val['info']['inn'] = $val['inn'];
            $business[] = $val['info'];
        }

        if ($inn === NULL)
            return $business;
        else {
            if (count($business) == 1) return $business[0];
            else return $business;
        }
		
    }

    /* Добавление нового */
	public function add($data) 
	{
        
        $return = array();
	
		/* Проверяем указана ли ИНН */
		if (!array_key_exists('inn',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан ИНН';
			return $return;
		}

        $inn = $data['inn'];
        unset($data['inn']);
        $info = $data;

		/* Удаляем из строки лишние символы кроме цифр */
		$inn = preg_replace('~\D+~','', $inn);

		/* Проверяем ИНН валидатором */
		if ( (strlen($inn) != 10) and (strlen($inn) != 12) ) {
			$return['error'] = true;
			$return['message'] = 'Неверно указан ИНН';
			return $return;
		}


		/* Ищем дубликаты */
        $select = $this->db->query('SELECT * FROM business WHERE inn = ?', $inn);
        
		if ($select->numRows() > 0)  {
			$return['error'] = true;
			$return['message'] = 'Компания с ИНН '.$inn.' уже зарегистрирована в системе';
			return $return;
		}

        /* Добавляем запись о новой компании в БД */
        $sql_vars = array(
            $inn,
            json_encode($info, JSON_UNESCAPED_UNICODE)
        );

		$insert = $this->db->query('INSERT INTO business (inn, info) VALUES (?,?)', $sql_vars);
		
		if ($insert->affectedRows() == 1)
			return $this->all($inn);
		else  {
			$return['error'] = true;
			$return['message'] = 'Произошла ошибка при записи данных в БД';
			return $return;
		}
		
    }

    /* Изменение данных */
	public function update($inn, $data)
	{  

        if ($inn === NULL) {
            $return['error'] = true;
			$return['message'] = 'Не указан ИНН компании';
			return $return; 
        }

        /* Удаляем из строки лишние символы кроме цифр */
		$inn = preg_replace('~\D+~','', $inn);

		/* Проверяем ИНН валидатором */
		if ( (strlen($inn) != 10) and (strlen($inn) != 12) ) {
			$return['error'] = true;
			$return['message'] = 'Неверно указан ИНН';
			return $return;
        }
        
		/* Проверяем, есть ли клиент с таким ИНН */
		$select = $this->db->query('SELECT * FROM business WHERE inn = ?', $inn);
		if ($select->numRows() == 0) {
			$return['error'] = true;
			$return['message'] = 'Компания с ИНН '.$inn.' не найдена в системе';
			return $return;
		}


        $company = $select->fetchArray();
        
        $companyInfo = json_decode($company['info'], true);
		
		$sql_vars = array();
		
		$sql = 'UPDATE business SET ';
		
        $sql .= 'info = ?, ';
        $sql_vars[] = json_encode($data, JSON_UNESCAPED_UNICODE);
        
		$sql = mb_substr($sql, 0, -2) . ' WHERE (inn = ?)';
		$sql_vars[] = $inn;

		/* Отправляем запрос в БД на изменение */
		$update = $this->db->query($sql, $sql_vars);

		/* Возвращаем обновленные данные из БД */
		return $this->all($inn);

	}
    
}
