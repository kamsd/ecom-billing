<?php

namespace Billing;

/* Клиенты */

class Clients
{

    /* Constructor */
	
	public function __construct()
	{
        $this->db = new \Billing\DB(); // подключение к БД
    }
	
	/* Получение списка клиентов */
	public function all($inn = NULL) 
	{
        
        $clients = array();

        if ($inn === NULL) {
            $select = $this->db->query('SELECT * FROM clients ORDER BY id DESC');
        } 
        else {
            $select = $this->db->query('SELECT * FROM clients WHERE inn = ? LIMIT 1', $inn);
        }

        $all = $select->fetchAll();

        foreach ($all as $client) {
            $client['info'] = json_decode($client['info'], true);
            $clients[] = $client;
        }

        if ($inn === NULL)
            return $clients;
        else {
            if (count($clients) == 1) return $clients[0];
            else return $clients;
        }
		
    }

    /* Добавление нового клиента */
	public function add($data) 
	{
        
        $return = array();
	
		/* Проверяем указана ли ИНН */
		if (!array_key_exists('inn',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан ИНН клиента';
			return $return;
		}

        $inn = $data['inn'];
        unset($data['inn']);
        $info = $data;

		/* Удаляем из строки лишние символы кроме цифр */
		$inn = preg_replace('~\D+~','', $inn);

		/* Проверяем ИНН валидатором */
		if ( (strlen($inn) != 10) and (strlen($inn) != 12) ) {
			$return['error'] = true;
			$return['message'] = 'Неверно указан ИНН';
			return $return;
		}


		/* Ищем дубликаты */
        $select = $this->db->query('SELECT * FROM clients WHERE inn = ?', $inn);
        
		if ($select->numRows() > 0)  {
			$return['error'] = true;
			$return['message'] = 'Клиент с ИНН '.$inn.' уже зарегистрирован в системе';
			return $return;
		}

		if (count($info) == 0) $info = null;
		else $info = json_encode($info, JSON_UNESCAPED_UNICODE);

        /* Добавляем запись о новом клиенте в БД */
        $sql_vars = array(
            $inn,
            $info
        );

		$insert = $this->db->query('INSERT INTO clients (inn, info) VALUES (?,?)', $sql_vars);
		
		if ($insert->affectedRows() == 1)
			return $this->all($inn);
		else  {
			$return['error'] = true;
			$return['message'] = 'Произошла ошибка при записи данных в БД';
			return $return;
		}
		
    }

    /* Изменение данных о клиенте */
	public function update($inn, $data)
	{  

        if ($inn === NULL) {
            $return['error'] = true;
			$return['message'] = 'Не указан ИНН клиента';
			return $return; 
        }

        /* Удаляем из строки лишние символы кроме цифр */
		$inn = preg_replace('~\D+~','', $inn);

		/* Проверяем ИНН валидатором */
		if ( (strlen($inn) != 10) and (strlen($inn) != 12) ) {
			$return['error'] = true;
			$return['message'] = 'Неверно указан ИНН';
			return $return;
        }
        
		/* Проверяем, есть ли клиент с таким ИНН */
		$select = $this->db->query('SELECT * FROM clients WHERE inn = ?', $inn);
		if ($select->numRows() == 0) {
			$return['error'] = true;
			$return['message'] = 'Клиент с ИНН '.$inn.' не найден в системе';
			return $return;
		}


		//$userinfo = $select->fetchArray();
		
		$sql_vars = array();
		
		$sql = 'UPDATE clients SET ';
		
		/* Собираем поля, которые надо обновить */

		if (array_key_exists('info', $data))
		{
		  $sql .= 'info = ?, ';
		  $sql_vars[] = json_encode($data['info'], JSON_UNESCAPED_UNICODE);
        }

		if (array_key_exists('status', $data))
		{
		  $sql .= 'status = ?, ';
		  $sql_vars[] = $data['status'];
		}

		if (array_key_exists('balance', $data))
		{
		  $sql .= 'balance = ?, ';
		  $sql_vars[] = $data['balance'];
		}
		
		if (count($sql_vars) == 0) {
			$return['error'] = true;
			$return['message'] = 'Не указаны данные для изменения';
			return $return;
		}

		$sql = mb_substr($sql, 0, -2) . ' WHERE (inn = ?)';
		$sql_vars[] = $inn;

		/* Отправляем запрос в БД на изменение */
		$update = $this->db->query($sql, $sql_vars);

		/* Возвращаем обновленные данные из БД */
		return $this->all($inn);

	}

	/* Изменение внутреннего баланса клиента */
	public function changeBalance($inn, $source, $order_id, $value, $title, $date_start)
	{
		$client_info = $this->all($inn);
		$current_balance = $client_info['balance'] + $value;
		$this->update($inn, array('balance' => $current_balance));

		//Добавляем запись в историю транзакций
		$transaction = (new \Billing\Transactions())
			->add(
				$inn,
				$source,
				$order_id,
				$title,
				$value,
				$current_balance,
				$date_start
			);

	}
    
}
