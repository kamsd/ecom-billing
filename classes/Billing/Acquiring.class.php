<?php

namespace Billing;

class Acquiring
{

    public function __construct()
	{
        $this->db = new \Billing\DB();
    }

    public function generatePayLink($uid, $data)
    {
        
        $description = "Оплата заказа " . $uid;
        $DATA = new \stdClass();
        $DATA->Email = $data['email'];
        $items = [];
        $sum = 0;
        foreach ($data['positions'] as $p) {
            $items[] = [
                "Name" => $p['title'],
                "Price"=> $p['price'],
                "Quantity" => $p['amount'],
                "Amount" => $p['total'],
                "PaymentMethod" => "full_payment",
                "PaymentObject" => $p['type'] == 0 ? "service" : "commodity",
                "Tax" => "none"
            ];
            $sum += $p['total'];
        }
        

        $Receipt = new \stdClass();
        $Receipt->Email = $data['email'];
        $Receipt->EmailCompany = "sales@ecomkassa.ru";
        $Receipt->Taxation = "usn_income";
        $Receipt->Items = $items;

        if (!TINKOFF_TERMINAL_KEY)
            return $this->error("Не настроен терминал оплаты");

        $response = $this->sendResponse([
                "TerminalKey" => TINKOFF_TERMINAL_KEY,
                "Amount" => $sum,
                "OrderId" => $uid,
                "Description" => $description,
                "DATA" => $DATA,
                "Receipt" => $Receipt,
                "Items" => $items
            ]);

        
        if ($response) {
            if (isset($response['PaymentURL']))
                return ["PaymentURL" => $response['PaymentURL']];
            else return $this->error("При создании платежа произошла ошибка. Пожалуйста, попробуйте позже");
        } else
            return $this->error("При создании платежа произошла ошибка. Пожалуйста, попробуйте позже");
    }

    private function sendResponse($data) {
        $data_string = json_encode($data);
        $ch = curl_init("https://securepay.tinkoff.ru/v2/Init");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
            )
        );
        $result = curl_exec($ch);

        json_decode($result);
        if (json_last_error() == JSON_ERROR_NONE) {
            $return = json_decode($result, true);
            if (isset($return['PaymentURL'])) return $return;
            elseif (isset($return['errorMessage'])) {
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function insert($data) {

        if (!isset($data['OrderId']))
            return false;
            
        if (isset($data['Status']) && $data['Status'] == 'CONFIRMED') {
            $select = $this->db->query('SELECT id, status, method FROM invoices WHERE uid = ? ORDER BY id DESC LIMIT 1', $data['OrderId'])->fetchArray();
            
            if ($select['status'] == 0 && $select['method'] == 'card') {          
                $resp = (new \Billing\Invoices())->update(
                    $select['id'],['status' => 1]
                );
            }
            
        }

        $this->db->query('INSERT INTO acquiring (order_id, payment_id, status, data) VALUES (?,?,?,?)',
            [
                $data['OrderId'],
                $data['PaymentId'],
                NULL,
                json_encode($data, JSON_UNESCAPED_UNICODE)
            ]);

        return 'OK';

    }

    private function error($message)
    {
        return array(
            "error" => true,
            "message" => $message
        );
    }

}