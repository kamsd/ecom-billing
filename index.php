<?php

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
//error_reporting(E_ALL);

// Отключение вывода любых ошибок
error_reporting(0);

// Подключение Composer
require_once __DIR__.'/vendor/autoload.php';

// Конфигурационный файл
if (file_exists(__DIR__.'/configs/config.prod.php')) {
    require_once __DIR__.'/configs/config.prod.php';
} elseif (file_exists(__DIR__.'/configs/config.local.php')) {
    require_once __DIR__.'/configs/config.local.php';
} else {
    die('Отсутствует файл конфигурации');
}

// Подключение всех классов
spl_autoload_register(function ($class) {
    $file_of_class = __DIR__.'/classes/'.str_replace('\\', '/', $class).'.class.php';
    if (file_exists($file_of_class)) {
        require_once $file_of_class;
    }
});

session_start();


// Маршрутизация
$klein = new \Klein\Klein();

$klein->respond('/?', function ($request) {
    
});

$klein->respond(array('GET','POST','PUT'),'/api/v1/[:name]?/[:parameter]?', function ($request) {
    $name = $request->name;
    $parameter = $request->parameter;
    require_once __DIR__.'/api.php';
});

$klein->dispatch();