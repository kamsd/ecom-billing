<?php

namespace Billing;

/* Услуги */

class Services
{

    /* Constructor */
	
	public function __construct()
	{
        $this->db = new \Billing\DB(); // подключение к БД
    }
	
	/* Получение списка услуг */
	public function all(
        $id = NULL, // ID услуги
        $data = NULL // допольнительные параметры, фильтр
    ) 
    {
        
        $services = array();

        if ($id === NULL) {
            $select = $this->db->query('SELECT * FROM services ORDER BY id DESC');
        } 
        else {
            $select = $this->db->query('SELECT * FROM services WHERE id = ? LIMIT 1', $id);
        }

        $services = $select->fetchAll();

        if ($id === NULL)
            return $services;
        else {
            if (count($services) == 1) return $services[0];
            else return $services;
        }
		
    }

    /* Добавление новой услуги */
	public function add($data) 
	{
        
        $return = array();
	
        /* Проверяем указана ли обязательные параметры */
		
		
		if (!array_key_exists('provider',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан поставщик услуги или товара "provider"';
			return $return;
		}

		if (!is_int($data['provider'])) {
            $return['error'] = true;
			$return['message'] = 'Поставщик "provider" имеет недопустимое значение';
			return $return;
		}

		/* Ищем поставщика в БД */
		$provider = (new \Billing\Business())->all($data['provider']);
				
		if (count($provider) == 0)  {
			$return['error'] = true;
			$return['message'] = 'Поставщик с ИНН '.$data['provider'].' не найден в системе';
			return $return;
		}
		

		if (!array_key_exists('title',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указано название услуги';
			return $return;
        }
        
        if (!array_key_exists('period',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан период действия услуги';
			return $return;
		}
		
		if (!array_key_exists('type',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан тип записи (услуга или товар)';
			return $return;
		}
		
		if (!is_int($data['type'])) {
            $return['error'] = true;
			$return['message'] = 'Тип "type" имеет недопустимое значение';
			return $return;
		}

		if (!array_key_exists('nds',$data)) {
            $data['nds'] = NULL;
		}

		if ( (!is_null($data['nds'])) and ($data['nds'] != 0) and ($data['nds'] != 10) and ($data['nds'] != 20) ) {
            $return['error'] = true;
			$return['message'] = 'НДС "nds" имеет недопустимое значение';
			return $return;
		}

		if (!array_key_exists('measure',$data)) {
            $return['error'] = true;
			$return['message'] = 'Не указана мера измерения "measure" товара или услуги';
			return $return;
		}
		
        if (!is_int($data['period'])) {
            $return['error'] = true;
			$return['message'] = 'Период действия услуги "period" имеет недопустимое значение';
			return $return;
        }

        if (!array_key_exists('cost',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указана стоимость услуги';
			return $return;
        }
        
        if (!is_int($data['cost'])) {
            $return['error'] = true;
			$return['message'] = 'Стоимость услуги "cost" имеет недопустимое значение';
			return $return;
        }

        if (!array_key_exists('description',$data)) {
            $data['description'] = NULL;
		}
		
		if ( ($data['type'] == 1) and ($data['period'] != 0) ) {
			$return['error'] = true;
			$return['message'] = 'Товар не может иметь период "period" отличный от 0';
			return $return;
		}

        /* Добавляем запись о новой услуги в БД */
        $sql_vars = array(
			$data['provider'],
            $data['title'],
            $data['description'],
            $data['type'],
            $data['period'],
			$data['cost'],
			$data['nds'],
			$data['measure']
        );

		$insert = $this->db->query('INSERT INTO services (provider, title, description, type, period, cost, nds, measure) VALUES (?,?,?,?,?,?,?,?)', $sql_vars);
		
		if ($insert->affectedRows() == 1)
			return $this->all($insert->lastInsertId());
		else  {
			$return['error'] = true;
			$return['message'] = 'Произошла ошибка при записи данных в БД';
			return $return;
		}
		
    }

    /* Изменение данных об услуге */
	public function update($id = NULL, $data)
	{  

        if ($id === NULL) {
            $return['error'] = true;
			$return['message'] = 'Не указан ID услуги';
			return $return; 
        }

		/* Проверяем, есть ли услуга с таким ID */
		$select = $this->db->query('SELECT * FROM services WHERE id = ?', $id);
		if ($select->numRows() == 0) {
			$return['error'] = true;
			$return['message'] = 'Услуга ID '.$id.' не найдена в системе';
			return $return;
		}
		
		$sql_vars = array();
		
		$sql = 'UPDATE services SET ';
		
		/* Собираем поля, которые надо обновить */

		if (array_key_exists('status', $data))
		{
		  $sql .= 'status = ?, ';
		  $sql_vars[] = $data['status'];
		}
		
		if (count($sql_vars) == 0) {
			$return['error'] = true;
			$return['message'] = 'Не указаны данные для изменения';
			return $return;
		}

		$sql = mb_substr($sql, 0, -2) . ' WHERE (id = ?)';
        $sql_vars[] = $id;

		/* Отправляем запрос в БД на изменение */
		$update = $this->db->query($sql, $sql_vars);

		/* Возвращаем обновленные данные из БД */
		return $this->all($id);

	}
    
}
