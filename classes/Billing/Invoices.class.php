<?php

namespace Billing;

/* Счета */

class Invoices
{

    /* Constructor */
	
	public function __construct()
	{
        $this->db = new \Billing\DB(); // подключение к БД
    }
	
	/* Получение списка счетов */
	public function all(
        $id = NULL, // ID счета
        $data = array() // допольнительные параметры, фильтр
    )
    {
        
        $incoices = array();

        if ($id != NULL) {
            $select = $this->db->query('SELECT * FROM invoices WHERE id = ? LIMIT 1', $id);
        } 
        else {

            $sql_vars = array();
		
            $sql = 'SELECT * FROM invoices WHERE ';
			
			/* Собираем поля, по которым надо искать */

			if (array_key_exists('inn', $data))
			{
				$sql .= 'inn = ? AND ';
				$sql_vars[] = $data['inn'];
            }

            if (array_key_exists('uid', $data))
			{
				$sql .= 'uid = ? AND ';
				$sql_vars[] = $data['uid'];
            }

            if (array_key_exists('status', $data))
			{
				$sql .= 'status = ? AND ';
				$sql_vars[] = $data['status'];
            }

            if (array_key_exists('method', $data))
			{
				$sql .= 'method = ? AND ';
				$sql_vars[] = $data['method'];
            }

            if (array_key_exists('date_start', $data))
			{
                if ($this->validateDateTime($data['date_start'])) {
                    $sql .= 'updated >= ? AND ';
                    $sql_vars[] = $data['date_start'];
                }
            }

            if (array_key_exists('date_end', $data))
			{
                if ($this->validateDateTime($data['date_end'])) {
                    $sql .= 'updated <= ? AND ';
                    $sql_vars[] = $data['date_end'];
                }
            }
            

			if (count($sql_vars) > 0) {
                $sql = mb_substr($sql, 0, -5);
                $select = $this->db->query($sql, $sql_vars);
                $sql .= ' ORDER BY id DESC';
			} else {
                $sql = mb_substr($sql, 0, -7);
                $select = $this->db->query($sql);
                $sql .= ' ORDER BY id DESC';
			}

        }

        $all = $select->fetchAll();

        foreach ($all as $incoice) {
            $incoice['services'] = json_decode($incoice['services'], true);
            $incoice['documents'] = json_decode($incoice['documents'], true);
            $incoices[] = $incoice;
        }

        if ($id === NULL)
            return $incoices;
        else {
            if (count($incoices) == 1) return $incoices[0];
            else return $incoices;
        }
		
    }

    /* Создание нового счета */
	public function add($data) 
	{

        $return = array();

		/* Проверяем указан ли ИНН */
		if (!array_key_exists('inn',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан ИНН клиента';
			return $return;
		}

        $inn = $data['inn'];
        unset($data['inn']);

		/* Удаляем из строки лишние символы кроме цифр */
		$inn = preg_replace('~\D+~','', $inn);

		/* Проверяем ИНН валидатором */
		if ( (strlen($inn) != 10) and (strlen($inn) != 12) ) {
			$return['error'] = true;
			$return['message'] = 'Неверно указан ИНН';
			return $return;
		}

		/* Ищем клиента в БД */
        $select = $this->db->query('SELECT * FROM clients WHERE inn = ?', $inn);
        
		if ($select->numRows() == 0)  {
			$return['error'] = true;
			$return['message'] = 'Клиент с ИНН '.$inn.' не найден в системе';
			return $return;
        }
        

        /* Проверяем указан ли массив услуг */
		if (!array_key_exists('services',$data)) {
			$return['error'] = true;
			$return['message'] = 'Не указан массив услуг "services"';
			return $return;
        }

		if ( (!is_array($data['services'])) or (count($data['services']) == 0) or (!$data['services'][0]) )  {
			$return['error'] = true;
			$return['message'] = 'Массив услуг "services" пуст или задан неверно';
			return $return;
        }
        
        $services = array();
        $list_services = array();

        $all_services = (new \Billing\Services())->all();
        foreach ($all_services as $serv) {
            $list_services[$serv['id']] = $serv;
        }

        foreach ($data['services'] as $service) {

            /* Проверяем корректность указания даных */

            if (!array_key_exists('id',$service)) {
                $return['error'] = true;
                $return['message'] = 'Не указан идентификатор услуги "id" в массиве услуг';
                return $return;
            }

            if (!is_int($service['id'])) {
                $return['error'] = true;
                $return['message'] = 'Идентификатор услуги "id" имеет недопустимое значение';
                return $return;
            }

            if (!array_key_exists($service['id'], $list_services)) {
                $return['error'] = true;
                $return['message'] = 'Идентификатор услуги "id" не найден в системе';
                return $return;
            }


            if (!array_key_exists('amount',$service)) {
                $return['error'] = true;
                $return['message'] = 'Не указано количество единиц услуги "amount" в массиве услуг';
                return $return;
            }

            if (!is_int($service['amount'])) {
                $return['error'] = true;
                $return['message'] = 'Количество единиц услуги "amount" имеет недопустимое значение';
                return $return;
            }

            if (!array_key_exists('date_start',$service)) {
                $return['error'] = true;
                $return['message'] = 'Не указана дата начала действия услуги "date_start" в массиве услуг';
                return $return;
            }

            if (!$this->validateDate($service['date_start'])) {
                $return['error'] = true;
                $return['message'] = 'Дата начала действия услуги "date_start" имеет недопустимое значение';
                return $return;
            }

            if (strtotime($service['date_start']) < strtotime('-1 day')) {
                $return['error'] = true;
                $return['message'] = 'Дата начала действия услуги "date_start" не может быть в прошлом';
                return $return;
            }

            $services[] = array(
                'id' => $service['id'],
                'amount' => $service['amount'],
                'date_start' => $service['date_start']
            );

        }

        $uid = date('ymdHi').mt_rand(10,99);



        if (!isset($data['method']) || $data['method'] == 'invoice') {

            $invoices = $this->genDocument($uid, $inn, $services, 'invoice');

            if (isset($invoices['error'])) return $invoices;

            $documents['invoices'] = array();

            foreach ($invoices as $invoice) {
                $documents['invoices'][] = $invoice;
            }
            $method = "invoice";

        } elseif (isset($data['method']) || $data['method'] == 'card') {

            $payment = $this->payCard($uid, $inn, $services);
            if (isset($payment['error'])) return $payment;
            $documents['paymentURL'] = $payment['PaymentURL'];
            $method = "card";

        } else {
            return [
                'error' => true,
                'message' => 'Параметр method имеет недопустимое значение'
            ];
        }
        

        /* Добавляем запись о новом счете в БД */
        $sql_vars = array(
            $uid,
            $inn,
            json_encode($services, JSON_UNESCAPED_UNICODE),
            json_encode($documents, JSON_UNESCAPED_UNICODE),
            $method
        );

        $insert = $this->db->query('INSERT INTO invoices (uid, inn, services, documents, method) VALUES (?,?,?,?,?)', $sql_vars);
        
        if ($insert->affectedRows() == 1)
            return $this->all($insert->lastInsertId());
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        }

		
    }

    public function payCard($uid, $inn, $services) {
        $positions = [];

        foreach ($this->collect_services($services) as $collect_services) {
            foreach ($collect_services as $pos) {
                $positions[] = [
                    "title" => $pos["title"],
                    "amount" => $pos["amount"],
                    "price" => $pos["price"]*100,
                    "total" => $pos["total"]*100,
                    "type" => $pos["type"]
                ];
            }
        }

        $client_info = (new \Billing\Clients())->all($inn);

        return (new \Billing\Acquiring())->generatePayLink($uid, ["email" => $client_info['info']['email'], "positions" => $positions]);

    }

    

    /* Изменение статуса счета */
	public function update($id, $data)
	{  

        if ($id === NULL) {
            $return['error'] = true;
			$return['message'] = 'Не указан ID счета';
			return $return; 
        }

        /* Проверяем, есть ли счета с таким ID */
        
        $invoice = $this->all($id);
		if (count($invoice) == 0) {
			$return['error'] = true;
			$return['message'] = 'Счет с ID '.$id.' не найден в системе';
			return $return;
        }
		
		$sql_vars = array();
		
		$sql = 'UPDATE invoices SET ';
		
		/* Собираем поля, которые надо обновить */

		if (array_key_exists('status', $data))
		{
		  $sql .= 'status = ?, ';
		  $sql_vars[] = $data['status'];
		}
		
		if (count($sql_vars) == 0) {
			$return['error'] = true;
			$return['message'] = 'Не указаны данные для изменения';
			return $return;
        }
        

        if ( ($invoice['status'] == 1) and (in_array($data['status'], array(0,1,2))) )
        {
            $return['error'] = true;
			$return['message'] = 'Счет уже оплачен';
			return $return; 
        }

        /*
        if ( ($invoice['status'] == 2) and (in_array($data['status'], array(0,1,2))) )
        {
            $return['error'] = true;
			$return['message'] = 'Счет был отменен, создайте новый';
			return $return; 
        }
        */

        $client_info =  (new \Billing\Clients())->all($invoice['inn']);

        // формируем массив заказанных услуг, на которые сразу нужно оформить акт
        $collects = $this->collect_services($invoice['services']);

        //Формируем отчетные документы (акт, накладная) для оплаченного счета
        if ($data['status'] == 1) {


            if ($invoice['method'] == 'invoice') {

                // формируем накладную на товары сразу
                $torgs = $this->genDocument($invoice['uid'], $invoice['inn'], $invoice['services'], 'torg');

                $services_now = array();
                foreach ($collects as $collect_services) {
                    foreach ($collect_services as $service) {
                        foreach ($invoice['services'] as $serv) {
                            if ($service['id'] == $serv['id']) {
                                if ($service['period'] == 0) {
                                    $services_now[] = $service;
                                }
                            }
                        }
                    }
                }

                $acts = $this->genDocument($invoice['uid'], $invoice['inn'], $services_now, 'act');


            } else {
                $acts = [];
                $torgs = [];
            }
            
            //Вычисляем сколько должны были заплатить по счету
            
            $total_invoice = 0;

            foreach ($collects as $collect_services) {
                foreach ($collect_services as $service) {
                    $total_invoice = $total_invoice + $service['total'];

                    foreach ($invoice['services'] as $serv) {
                        if ($service['id'] == $serv['id']) {
                            $period = $service['period'];
                            $amount = $serv['amount'];
                            $date_start = $serv['date_start'];
                        }
                    }
                    
                    //Присваиваем клиенту оплаченные услуги/товары
                    $record = (new \Billing\Orders())
                    ->add($invoice['inn'], $service['id'], $invoice['uid'], $date_start, $period, $amount);

                }
            }

            //Изменяем внутренний баланс клиента
            $balance_update = (new \Billing\Clients())
                ->changeBalance(
                    $invoice['inn'],
                    $invoice['uid'],
                    $invoice['uid'],
                    $total_invoice * 100,
                    'Оплата счета №'.$invoice['uid'],
                    date('Y-m-d H:i:s')
                );


            // Сразу списываем за разовые услуги и товары
            $writeOff = (new \Billing\Orders())->writeOff($invoice['inn'], true);

        }


        /////
        /////
        /////

        if (!array_key_exists('acts', $invoice['documents']))
            $invoice['documents']['acts'] = array();

        if (!array_key_exists('acts', $invoice['documents']))
            $invoice['documents']['torgs'] = array();
        
        foreach ($acts as $act) {
            $invoice['documents']['acts'][] = $act;
        }

        foreach ($torgs as $torg) {
            $invoice['documents']['torgs'][] = $torg;
        }

        //////
        //////
        //////

        $sql .= 'documents = ?, ';
		$sql_vars[] = json_encode($invoice['documents'], JSON_UNESCAPED_UNICODE);

		$sql = mb_substr($sql, 0, -2) . ' WHERE (id = ?)';
		$sql_vars[] = $id;

		/* Отправляем запрос в БД на изменение */
		$update = $this->db->query($sql, $sql_vars);

		/* Возвращаем обновленные данные из БД */
		return $this->all($id);

    }


    public function genDocument($uid, $inn, $services, $type) {

        $output = array();

        $client = (new \Billing\Clients())->all($inn);

        if (array_key_exists('error', $client))
            return $client;

        $info = $client['info'];

        $CLIENT_INN = $inn;

        if ( (array_key_exists('company', $info)) and ($info['company'] != '') ) {
            $CLIENT_COMPANY = $info['company'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указано наименование юридического лица в информации о клиенте'
            );
        }

        if (array_key_exists('kpp', $info)) {
            $CLIENT_KPP = $info['kpp'];
        } else {
            $CLIENT_KPP = '-';
        }

        if ( (array_key_exists('address', $info)) and ($info['address'] != '') ) {
            $CLIENT_ADDRESS = $info['address'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан юридический адрес в информации о клиенте'
            );
        }

        if ( (array_key_exists('email', $info)) and ($info['email'] != '') ) {
            $CLIENT_EMAIL = $info['email'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан контактный email клиента'
            );
        }

        if ( (!array_key_exists('payment_account', $info)) or ( (array_key_exists('payment_account', $info)) and ($info['payment_account'] != '') ) ) {
            $CLIENT_PAYMENT_ACCOUNT = $info['payment_account'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан расчетный счет клиента'
            );
        }

        if ( (!array_key_exists('bank_name', $info)) or ( (array_key_exists('bank_name', $info)) and ($info['bank_name'] != '') ) ) {
            $CLIENT_BANK_NAME = $info['bank_name'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан банк клиента'
            );
        }

        if ( (!array_key_exists('bank_bik', $info)) or ( (array_key_exists('bank_bik', $info)) and ($info['bank_bik'] != '') ) ) {
            $CLIENT_BANK_BIK = $info['bank_bik'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан БИК банка клиента'
            );
        }

        if ( (!array_key_exists('bank_corr_account', $info)) or ( (array_key_exists('bank_corr_account', $info)) and ($info['bank_corr_account'] != '') ) ) {
            $CLIENT_BANK_CORR_ACCOUNT = $info['bank_corr_account'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан корр. счет банка клиента'
            );
        }


        if ( (!array_key_exists('phone', $info)) or ( (array_key_exists('phone', $info)) and ($info['phone'] != '') ) ) {
            $CLIENT_PHONE = $info['phone'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан контактный номер телефона клиента'
            );
        }


        
        
        

        if ( (array_key_exists('address', $info)) and ($info['address'] != '') ) {
            $CLIENT_ADDRESS = $info['address'];
        } else {
            return array(
                'error' => true,
                'message' => 'Не указан юридический адрес в информации о клиенте'
            );
        }



        $INVOICE_NUMBER = date('ymdHi').mt_rand(10,99);

        $months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];  
        $month_key = date('n')-1;

        $INVOICE_DATE = date('d') . ' ' . $months[$month_key] . ' ' . date('Y');



        // Получаем массив услуг и товаров с разбивкой по юр лицам
        $collect = $this->collect_services($services);
        
        if (isset($collect['error'])) return $collect;
        ///////////
        ///////////
        //// 
        ///////////
        ///////////

        foreach ($collect as $key=>$collect_services) {

            $uid = $this->genUID();

            $business = (new \Billing\Business())->all($key);

            $INVOICE_BANK_NAME = $business['bank_name'];
            $INVOICE_BANK_BIK = $business['bank_bik'];
            $INVOICE_BANK_CORR_ACCOUNT = $business['bank_corr_account'];
            $INVOICE_PAYMENT_ACCOUNT = $business['payment_account'];
            $INVOICE_COMPANY = $business['company'];
            $INVOICE_INN = $business['inn'];
            $INVOICE_KPP = $business['kpp'];
            $INVOICE_ADDRESS = $business['address'];
            $INVOICE_EMAIL = $business['email'];
            $INVOICE_PHONE = $business['phone'];
            $INVOICE_HEAD = $business['head'];
            $INVOICE_HEAD_SHORT = $business['head_short'];


            if ((strlen($business['sign']) > 0) and (!is_null($business['sign']))) {
                $img = file_get_contents($_SERVER['DOCUMENT_ROOT'].$business['sign']);
                $INVOICE_SIGN = 'data:image/png;base64,'.base64_encode($img);
            } else {
                $INVOICE_SIGN = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
            }

            if ((strlen($business['seal']) > 0) and (!is_null($business['seal']))) {
                $img = file_get_contents($_SERVER['DOCUMENT_ROOT'].$business['seal']);
                $INVOICE_SEAL = 'data:image/png;base64,'.base64_encode($img);
            } else {
                $INVOICE_SEAL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
            }


            $INVOICE_ITEMS = '';

            $TORG12_ITEMS = '';
            $TORG_TOTAL_ARTICLES = 0;

            $it = 0;
            $INVOICE_TOTAL_NUMBER = 0;
            $TORG_TOTAL_NUMBER_WITH_NDS = 0;
            $TORG_TOTAL_NUMBER_WITHOUT_NDS = 0;

            $NDS_10_TOTAL = 0;
            $NDS_20_TOTAL = 0;

            foreach ($collect_services as $service) {

                if ($service['nds'] === NULL) {
                    $nds_title = 'без НДС';
                    $nds_val = '&nbsp;';
                    $nds_val_total = '&nbsp;';
                }
                else if ($service['nds'] == 0) {
                    $nds_title = 'НДС 0%';
                    $nds_val = '&nbsp;';
                    $nds_val_total = '&nbsp;';
                }
                else if ($service['nds'] == 10) {
                    $nds_title = 'НДС 10%';
                    $nds_val = number_format($service['total']*0.9, 2, '.', ' ');
                    $nds_val_total = number_format($service['total'] - $service['total']*0.9, 2, '.', ' ');

                    if ($service['type'] == 0) {
                        $NDS_10_TOTAL = $NDS_10_TOTAL + ($service['total'] - $service['total']*0.9);
                    }
                }
                else if ($service['nds'] == 20) {
                    $nds_title = 'НДС 20%';
                    $nds_val = number_format($service['total'] - $service['total']*0.8, 2, '.', ' ');

                    if ($service['type'] == 0) {
                        $NDS_20_TOTAL = $NDS_20_TOTAL + ($service['total'] - $service['total']*0.8);
                    }
                }

                else {
                    return array(
                        'error' => true,
                        'message' => 'Не корректно указана величина НДС'
                    );
                }


                if ($type == 'torg') {
                    if ($service['type'] != 1) continue;
                    $it++;
                    $TORG_TOTAL_ARTICLES = $it;

                    $TORG_TOTAL_NUMBER_WITH_NDS = $TORG_TOTAL_NUMBER_WITH_NDS + $service['total'];

                    if ($service['nds'] === NULL) {
                        $TORG_TOTAL_NUMBER_WITHOUT_NDS = $TORG_TOTAL_NUMBER_WITHOUT_NDS + $service['total']*1;
                    }
                    else if ($service['nds'] == 0) {
                        $TORG_TOTAL_NUMBER_WITHOUT_NDS = $TORG_TOTAL_NUMBER_WITHOUT_NDS + $service['total']*1;
                    }
                    else if ($service['nds'] == 10) {
                        $TORG_TOTAL_NUMBER_WITHOUT_NDS = $TORG_TOTAL_NUMBER_WITHOUT_NDS + $service['total']*0.9;
                    }
                    else if ($service['nds'] == 20) {
                        $TORG_TOTAL_NUMBER_WITHOUT_NDS = $TORG_TOTAL_NUMBER_WITHOUT_NDS + $service['total']*0.8;
                    }

                    $TORG12_ITEMS .= '
                    <tr>
                        <td style="text-align:center">'.$it.'</td> 
                        <td>'.$service['title'].'</td> 
                        <td>'.$service['vendor_code'].'</td>
                        <td style="text-align:center">'.$service['measure'].'</td>
                        <td style="text-align:center">'.$service['okei_code'].'</td>
                        <td>&nbsp;</td>  
                        <td>&nbsp;</td>    
                        <td>&nbsp;</td>    
                        <td>&nbsp;</td>
                        <td style="text-align:right">'.$service['amount'].'</td>      
                        <td style="text-align:right">'.number_format($service['price'], 2, '.', ' ').'</td>   
                        <td style="text-align:right">'.$nds_val.'</td>    
                        <td style="text-align:center">'.$nds_title.'</td>    
                        <td>'.$nds_val_total.'</td>    
                        <td style="text-align:right">'.number_format($service['total'], 2, '.', ' ').'</td>
                    </tr>
                    ';

                } else {

                    if ( ($service['type'] != 0) and ($type == 'act') ) continue;

                    $it++;

                    $INVOICE_TOTAL_NUMBER = $INVOICE_TOTAL_NUMBER + $service['total'];
                    
                    $INVOICE_ITEMS .= '
                    <tr>
                            <td align="center">'.$it.'</td>
                            <td align="left">'.$service['title'].'</td>
                            <td align="right">'.$service['amount'].'</td>
                            <td align="left">'.$service['measure'].'</td>
                            <td align="right">'.number_format($service['price'], 2, ',', ' ').'</td>
                            <td align="right">'.number_format($service['total'], 2, ',', ' ').'</td>
                        </tr>
                    ';

                }
                
            }

            if ($it == 0) continue;


            $INVOICE_NDS_10_STRING = '';
            $INVOICE_NDS_10_VAL = '';
            $INVOICE_NDS_20_STRING = '';
            $INVOICE_NDS_20_VAL = '';
            $INVOICE_NDS_NONE_STRING = '';
            $INVOICE_NDS_NONE_VAL = '';

            if ( ($type == 'invoice') or ($type == 'act') ) {

                if ($NDS_10_TOTAL != 0) {
                    $INVOICE_NDS_10_STRING = 'В том числе НДС 10%:';
                    $INVOICE_NDS_10_VAL = number_format($NDS_10_TOTAL, 2, ',', ' ');
                }

                if ($NDS_20_TOTAL != 0) {
                    $INVOICE_NDS_20_STRING = 'В том числе НДС 20%:';
                    $INVOICE_NDS_20_VAL = number_format($NDS_20_TOTAL, 2, ',', ' ');
                }

                if (($NDS_10_TOTAL == 0) and ($NDS_20_TOTAL == 0)) {
                    $INVOICE_NDS_NONE_STRING = 'Без налога (НДС)';
                    $INVOICE_NDS_NONE_VAL = '-';
                }

            }

            $INVOICE_TOTAL_ITEMS = $it;
            $INVOICE_TOTAL_WORDS = $this->sum2words($INVOICE_TOTAL_NUMBER);
            $INVOICE_TOTAL_NUMBER = number_format($INVOICE_TOTAL_NUMBER, 2, ',', ' ');
            

            $TORG_TOTAL_ITEMS_WORDS = $this->number2string($TORG_TOTAL_ARTICLES);
            $TORG_TOTAL_WORDS = mb_strtolower($this->sum2words($TORG_TOTAL_NUMBER_WITH_NDS));
            $TORG_TOTAL_NUMBER_WITH_NDS = number_format($TORG_TOTAL_NUMBER_WITH_NDS, 2, '.', ' ');
            $TORG_TOTAL_NUMBER_WITHOUT_NDS = number_format($TORG_TOTAL_NUMBER_WITHOUT_NDS, 2, '.', ' ');
            

            $folder_level_1 = substr($uid, 0, 1);
            $folder_level_2 = substr($uid, 1, 1);

            if (!file_exists(__DIR__.'/../../uploads')) {
                mkdir(__DIR__.'/../../uploads', 0777, true);
            }

            if (!file_exists(__DIR__.'/../../uploads/'.$folder_level_1)) {
                mkdir(__DIR__.'/../../uploads/'.$folder_level_1, 0777, true);
            }

            if (!file_exists(__DIR__.'/../../uploads/'.$folder_level_1.'/'.$folder_level_2)) {
                mkdir(__DIR__.'/../../uploads/'.$folder_level_1.'/'.$folder_level_2, 0777, true);
            }

            if ($type == 'invoice')
                $template = TEMPLATE_INVOICE;
            else if ($type == 'act')
                $template = TEMPLATE_ACT;
            else if ($type == 'torg')
                $template = TEMPLATE_TORG;
            else {
                return array(
                    'error' => true,
                    'message' => 'Не указан шаблон документа'
                );
            }

             

            $htmlBody = file_get_contents($template);

            $searchVal = array(
                '{{INVOICE_BANK_NAME}}',
                '{{INVOICE_BANK_BIK}}',
                '{{INVOICE_BANK_CORR_ACCOUNT}}',
                '{{INVOICE_PAYMENT_ACCOUNT}}',
                '{{INVOICE_COMPANY}}',
                '{{INVOICE_INN}}',
                '{{INVOICE_KPP}}',
                '{{INVOICE_ADDRESS}}',
                '{{INVOICE_EMAIL}}',
                '{{INVOICE_PHONE}}',
                '{{INVOICE_HEAD}}',
                '{{INVOICE_HEAD_SHORT}}',
                '{{CLIENT_COMPANY}}',
                '{{CLIENT_INN}}',
                '{{CLIENT_KPP}}',
                '{{CLIENT_ADDRESS}}',
                '{{CLIENT_EMAIL}}',
                '{{CLIENT_PHONE}}',
                '{{CLIENT_PAYMENT_ACCOUNT}}',
                '{{CLIENT_BANK_NAME}}',
                '{{CLIENT_BANK_BIK}}',
                '{{CLIENT_BANK_CORR_ACCOUNT}}',
                '{{INVOICE_DATE}}',
                '{{INVOICE_NUMBER}}',
                '{{INVOICE_TOTAL_WORDS}}',
                '{{INVOICE_ITEMS}}',
                '{{INVOICE_TOTAL_NUMBER}}',
                '{{INVOICE_TOTAL_ITEMS}}',
                '{{INVOICE_NDS_10_STRING}}',
                '{{INVOICE_NDS_10_VAL}}',
                '{{INVOICE_NDS_20_STRING}}',
                '{{INVOICE_NDS_20_VAL}}',
                '{{INVOICE_NDS_NONE_STRING}}',
                '{{INVOICE_NDS_NONE_VAL}}',
                '{{INVOICE_SIGN}}',
                '{{INVOICE_SEAL}}',
                '{{TORG_TOTAL_NUMBER_WITH_NDS}}',
                '{{TORG_TOTAL_NUMBER_WITHOUT_NDS}}',
                '{{TORG_TOTAL_ARTICLES}}',
                '{{TORG12_ITEMS}}',
                '{{TORG_TOTAL_WORDS}}',
                '{{TORG_TOTAL_ITEMS_WORDS}}'
            );

            
            $replaceVal = array(
                $INVOICE_BANK_NAME,
                $INVOICE_BANK_BIK,
                $INVOICE_BANK_CORR_ACCOUNT,
                $INVOICE_PAYMENT_ACCOUNT,
                $INVOICE_COMPANY,
                $INVOICE_INN,
                $INVOICE_KPP,
                $INVOICE_ADDRESS,
                $INVOICE_EMAIL,
                $INVOICE_PHONE,
                $INVOICE_HEAD,
                $INVOICE_HEAD_SHORT,
                $CLIENT_COMPANY,
                $CLIENT_INN,
                $CLIENT_KPP,
                $CLIENT_ADDRESS,
                $CLIENT_EMAIL,
                $CLIENT_PHONE,
                $CLIENT_PAYMENT_ACCOUNT,
                $CLIENT_BANK_NAME,
                $CLIENT_BANK_BIK,
                $CLIENT_BANK_CORR_ACCOUNT,
                $INVOICE_DATE,
                $INVOICE_NUMBER,
                $INVOICE_TOTAL_WORDS,
                $INVOICE_ITEMS,
                $INVOICE_TOTAL_NUMBER,
                $INVOICE_TOTAL_ITEMS,
                $INVOICE_NDS_10_STRING,
                $INVOICE_NDS_10_VAL,
                $INVOICE_NDS_20_STRING,
                $INVOICE_NDS_20_VAL,
                $INVOICE_NDS_NONE_STRING,
                $INVOICE_NDS_NONE_VAL,
                $INVOICE_SIGN,
                $INVOICE_SEAL,
                $TORG_TOTAL_NUMBER_WITH_NDS,
                $TORG_TOTAL_NUMBER_WITHOUT_NDS,
                $TORG_TOTAL_ARTICLES,
                $TORG12_ITEMS,
                $TORG_TOTAL_WORDS,
                $TORG_TOTAL_ITEMS_WORDS
            ); 



            $htmlBody = str_replace($searchVal, $replaceVal, $htmlBody);


            $dompdf = new \Dompdf\Dompdf([
                'fontDir' => __DIR__.'/../../fonts',//указываем путь к папке, в которой лежат скомпилированные шрифты
                'defaultFont' => 'arial',//делаем наш шрифт шрифтом по умолчанию
                'isRemoteEnabled' => true
                ]);

            $dompdf->loadHtml($htmlBody, 'UTF-8');

            if ($type == 'torg') {
                $dompdf->setPaper('A4', 'landscape');
            } else {
                $dompdf->setPaper('A4', 'portrait');
            }

            $dompdf->render();
            $pdf_body = $dompdf->output();

            

            if ($type == 'invoice') {
                file_put_contents(__DIR__.'/../../uploads/'.$folder_level_1.'/'.$folder_level_2.'/INVOICE-'.$uid.'.pdf', $pdf_body);
                $output[] = array(
                    'file' => '/uploads/'.$folder_level_1.'/'.$folder_level_2.'/INVOICE-'.$uid.'.pdf',
                    'created' => date('Y-m-d H:i:s')
                );
            }
            else if ($type == 'act')
            {
                file_put_contents(__DIR__.'/../../uploads/'.$folder_level_1.'/'.$folder_level_2.'/ACT-'.$uid.'.pdf', $pdf_body);
                $output[] = array(
                    'file' => '/uploads/'.$folder_level_1.'/'.$folder_level_2.'/ACT-'.$uid.'.pdf',
                    'created' => date('Y-m-d H:i:s')
                );
            }

            else if ($type == 'torg')
            {
                file_put_contents(__DIR__.'/../../uploads/'.$folder_level_1.'/'.$folder_level_2.'/TORG12-'.$uid.'.pdf', $pdf_body);
                $output[] = array(
                    'file' => '/uploads/'.$folder_level_1.'/'.$folder_level_2.'/TORG12-'.$uid.'.pdf',
                    'created' => date('Y-m-d H:i:s')
                );
            }

            else {
                return false;
            }

        }
        ///////////
        ///////////
        //// 
        ///////////
        ///////////

        return $output;
        

    }

    private function collect_services($services){

        $return = array();

        foreach ($services as $service) {

            $serv = array();

            $serv_info = (new \Billing\Services())->all($service['id']);

            /*
            if ( (array_key_exists('error', $serv_info)) or ($serv_info['status'] == 1) ) {
                return array(
                    'error' => true,
                    'message' => 'Услуга ID '.$service['id'].' не найдена или находится в архиве'
                );
            }
            */

            $serv['id'] = $service['id'];
            $serv['period'] = $serv_info['period'];
            $serv['type'] = $serv_info['type'];
            $serv['title'] = $serv_info['title'];
            $serv['amount'] = $service['amount'];
            $serv['price'] = $serv_info['cost'] / 100;
            $serv['total'] = $serv['price'] * $serv['amount'];
            $serv['vendor_code'] = $serv_info['vendor_code'];
            $serv['okei_code'] = $serv_info['okei_code'];
            $serv['nds'] = $serv_info['nds'];
            $serv['measure'] = $serv_info['measure'];

            if (array_key_exists('onetime', $service)) {
                //$serv['price'] = ceil(($serv_info['cost'] / 100) / $serv_info['period']);
                //$serv['total'] = ceil($serv['total'] / $serv_info['period']);
            }

            $return[$serv_info['provider']][] = $serv;

        }

        return $return;

    }


    private function sum2words($n) {
        $words=array(
            900=>'девятьсот',
            800=>'восемьсот',
            700=>'семьсот',
            600=>'шестьсот',
            500=>'пятьсот',
            400=>'четыреста',
            300=>'триста',
            200=>'двести',
            100=>'сто',
            90=>'девяносто',
            80=>'восемьдесят',
            70=>'семьдесят',
            60=>'шестьдесят',
            50=>'пятьдесят',
            40=>'сорок',
            30=>'тридцать',
            20=>'двадцать',
            19=>'девятнадцать',
            18=>'восемнадцать',
            17=>'семнадцать',
            16=>'шестнадцать',
            15=>'пятнадцать',
            14=>'четырнадцать',
            13=>'тринадцать',
            12=>'двенадцать',
            11=>'одиннадцать',
            10=>'десять',
            9=>'девять',
            8=>'восемь',
            7=>'семь',
            6=>'шесть',
            5=>'пять',
            4=>'четыре',
            3=>'три',
            2=>'два',
            1=>'один',
        );
     
        $level=array(
            4=>array('миллиард', 'миллиарда', 'миллиардов'),
            3=>array('миллион', 'миллиона', 'миллионов'),
            2=>array('тысяча', 'тысячи', 'тысяч'),
        );
     
        list($rub,$kop)=explode('.',number_format($n,2));
        $parts=explode(',',$rub);
     
        for($str='', $l=count($parts), $i=0; $i<count($parts); $i++, $l--) {
            if (intval($num=$parts[$i])) {
                foreach($words as $key=>$value) {
                    if ($num>=$key) {
                        // Fix для одной тысячи
                        if ($l==2 && $key==1) {
                            $value='одна';
                        }
                        // Fix для двух тысяч
                        if ($l==2 && $key==2) {
                            $value='две';
                        }
                        $str.=($str!=''?' ':'').$value;
                        $num-=$key;
                    }
                }
                if (isset($level[$l])) {
                    $str.=' '.$this->num2word($parts[$i],$level[$l]);
                }
            }
        }
     
        if (intval($rub=str_replace(',','',$rub))) {
            $str.=' '.$this->num2word($rub,array('рубль', 'рубля', 'рублей'));
        }
     
        $str.=($str!=''?' ':'').$kop;
        $str.=' '.$this->num2word($kop,array('копейка', 'копейки', 'копеек'));
     
        return mb_strtoupper(mb_substr($str,0,1,'utf-8'),'utf-8').
             mb_substr($str,1,mb_strlen($str,'utf-8'),'utf-8');
    }

    private function num2word($n,$words) {
        return ($words[($n=($n=$n%100)>19?($n%10):$n)==1?0 : (($n>1&&$n<=4)?1:2)]);
    }

    private function number2string($number) {
	
        // обозначаем словарь в виде статической переменной функции, чтобы 
        // при повторном использовании функции его не определять заново
        static $dic = array(
        
            // словарь необходимых чисел
            array(
                -2	=> 'две',
                -1	=> 'одна',
                1	=> 'один',
                2	=> 'два',
                3	=> 'три',
                4	=> 'четыре',
                5	=> 'пять',
                6	=> 'шесть',
                7	=> 'семь',
                8	=> 'восемь',
                9	=> 'девять',
                10	=> 'десять',
                11	=> 'одиннадцать',
                12	=> 'двенадцать',
                13	=> 'тринадцать',
                14	=> 'четырнадцать' ,
                15	=> 'пятнадцать',
                16	=> 'шестнадцать',
                17	=> 'семнадцать',
                18	=> 'восемнадцать',
                19	=> 'девятнадцать',
                20	=> 'двадцать',
                30	=> 'тридцать',
                40	=> 'сорок',
                50	=> 'пятьдесят',
                60	=> 'шестьдесят',
                70	=> 'семьдесят',
                80	=> 'восемьдесят',
                90	=> 'девяносто',
                100	=> 'сто',
                200	=> 'двести',
                300	=> 'триста',
                400	=> 'четыреста',
                500	=> 'пятьсот',
                600	=> 'шестьсот',
                700	=> 'семьсот',
                800	=> 'восемьсот',
                900	=> 'девятьсот'
            ),
            
            // словарь порядков со склонениями для плюрализации
            array(
                array('', '', ''),
                array('тысяча', 'тысячи', 'тысяч'),
                array('миллион', 'миллиона', 'миллионов'),
                array('миллиард', 'миллиарда', 'миллиардов'),
                array('триллион', 'триллиона', 'триллионов'),
                array('квадриллион', 'квадриллиона', 'квадриллионов'),
                // квинтиллион, секстиллион и т.д.
            ),
            
            // карта плюрализации
            array(
                2, 0, 1, 1, 1, 2
            )
        );
        
        // обозначаем переменную в которую будем писать сгенерированный текст
        $string = array();
        
        // дополняем число нулями слева до количества цифр кратного трем,
        // например 1234, преобразуется в 001234
        $number = str_pad($number, ceil(strlen($number)/3)*3, 0, STR_PAD_LEFT);
        
        // разбиваем число на части из 3 цифр (порядки) и инвертируем порядок частей,
        // т.к. мы не знаем максимальный порядок числа и будем бежать снизу
        // единицы, тысячи, миллионы и т.д.
        $parts = array_reverse(str_split($number,3));
        
        // бежим по каждой части
        foreach($parts as $i=>$part) {
            
            // если часть не равна нулю, нам надо преобразовать ее в текст
            if($part>0) {
                
                // обозначаем переменную в которую будем писать составные числа для текущей части
                $digits = array();
                
                // если число треххзначное, запоминаем количество сотен
                if($part>99) {
                    $digits[] = floor($part/100)*100;
                }
                
                // если последние 2 цифры не равны нулю, продолжаем искать составные числа
                // (данный блок прокомментирую при необходимости)
                if($mod1=$part%100) {
                    $mod2 = $part%10;
                    $flag = $i==1 && $mod1!=11 && $mod1!=12 && $mod2<3 ? -1 : 1;
                    if($mod1<20 || !$mod2) {
                        $digits[] = $flag*$mod1;
                    } else {
                        $digits[] = floor($mod1/10)*10;
                        $digits[] = $flag*$mod2;
                    }
                }
                
                // берем последнее составное число, для плюрализации
                $last = abs(end($digits));
                
                // преобразуем все составные числа в слова
                foreach($digits as $j=>$digit) {
                    $digits[$j] = $dic[0][$digit];
                }
                
                // добавляем обозначение порядка или валюту
                $digits[] = $dic[1][$i][(($last%=100)>4 && $last<20) ? 2 : $dic[2][min($last%10,5)]];
                
                // объединяем составные числа в единый текст и добавляем в переменную, которую вернет функция
                array_unshift($string, join(' ', $digits));
            }
        }
        
        // преобразуем переменную в текст и возвращаем из функции, ура!
        return join(' ', $string);
    }

    private function genUID()
    {
        $uid = uniqid(NULL, TRUE);
        $rawid = strtoupper(sha1(uniqid(rand(), true)));
        $result = substr($uid, 6, 8);
        $result .= '-';
        $result .= substr($uid, 0, 4);
        $result .= '-';
        $result .= substr(sha1(substr($uid, 3, 3)), 0, 4);
        $result .= '-';
        $result .= substr(sha1(substr(time(), 3, 4)), 0, 4);
        $result .= '-';
        $result .= strtolower(substr($rawid, 10, 12));
        return $result;
    }

    public function invoices_update()
    {
        $select = $this->db->query('SELECT * FROM invoices WHERE status = 0 AND created <= "'.date('Y-m-d H:i:s', strtotime('-5 days', strtotime('now'))).'"');
        $invoices = $select->fetchAll();

        if (count($invoices) == 0) {

            return array(
                'status' => 'success',
                'message' => 'Неоплаченных счетов с датой выставления более 5 дней нет'
            );

        } else {

            $amount = 0;
            $upd = array();
            foreach($invoices as $invoice) {
                $amount++;
                $upd[] = $invoice['id'];
            }

            $select = $this->db->query('UPDATE invoices SET status = 2 WHERE id IN ('.implode(", ",$upd).')');

            return array(
                'status' => 'success',
                'message' => 'Неоплаченные счета с датой выставления более 5 дней отменены. Найдено: '.$amount
            );
        }
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    private function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    private function error($message)
    {
        return array(
            "error" => true,
            "message" => $message
        );
    }
    
}
