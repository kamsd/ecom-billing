<?php

////
// ** Параметры MySQL: ** //
////

/** Имя базы данных */
define('DB_NAME', 'ecom-billing');

/** Имя пользователя MySQL */
define('DB_USER', 'ecomkassa');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'kIW8Q3lIy1yc');

/** Имя сервера MySQL */
define('DB_HOST', 'rc1a-2crpby5ccqtcnd4l.mdb.yandexcloud.net');

/** Путь к client-key.pem */
define('DB_SSL_CLIENT_KEY', NULL);

/** Путь к client-cert.pem */
define('DB_SSL_CLIENT_CERT', NULL);

/** Путь к ca.pem */
define('DB_SSL_CA', __DIR__.'/../../../../root/.mysql/root.crt');


////////////////////////
////////////////////////
////////////////////////


////
// ** Шаблоны документов: ** //
////

/** Шаблон счета */
define('TEMPLATE_INVOICE', __DIR__.'/../templates/invoice_default.html');

/** Шаблон акта */
define('TEMPLATE_ACT', __DIR__.'/../templates/act_default.html');

/** Шаблон накладной */
define('TEMPLATE_TORG', __DIR__.'/../templates/torg12_default.html');

////////////////////////
////////////////////////
////////////////////////


////
// ** Реквизиты организации (для выставления счета): ** //
////

/** Название банка */
define('INVOICE_BANK_NAME', 'ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ" ');

/** БИК банка */
define('INVOICE_BANK_BIK', '044525999');

/** Корр. счёт банка */
define('INVOICE_BANK_CORR_ACCOUNT', '30101810845250000999');

/** Расчётный счет */
define('INVOICE_PAYMENT_ACCOUNT', '40702810701500012427');

/** Наименование юридического лица (поставщика) */
define('INVOICE_COMPANY', 'ООО "ЗЕ ПОИНТ"');

/** ИНН юридического лица (поставщика) */
define('INVOICE_INN', '7708317992');

/** КПП юридического лица (поставщика) */
define('INVOICE_KPP', '770801001');

/** Юридический адрес поставщика */
define('INVOICE_ADDRESS', '101000 г. Москва, пер. Уланский, д. 13 стр. 4 пом. 1');

/** Генеральный директор поставщика */
define('INVOICE_HEAD', 'Носов Александр Сергеевич');
define('INVOICE_HEAD_SHORT', 'Носов А.С.');

/** Подпись поставщика */
define('INVOICE_SIGN', __DIR__.'/../templates/sign_default.png');

/** Печать поставщика */
define('INVOICE_SEAL', __DIR__.'/../templates/seal_default.png');

/** Контактный адрес электронной почты поставщика */
define('INVOICE_EMAIL', 'sales@ecomkassa.ru');

/** Контактный номер телефона поставщика */
define('INVOICE_PHONE', '+79689363395');

/** Терминал интернет-эквайринга Тинькофф для оплаты картой */
define('TINKOFF_TERMINAL_KEY', '1631636111895DEMO');








