<?php

// Приемник API-запросов

header("HTTP/1.1 200 OK");
header('Content-Type: application/json; charset=utf-8');
ini_set('default_charset', 'utf-8');

if (!function_exists('getallheaders')) {
    function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
    }
}

$headers = getallheaders();

$return = array();
$error = array();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $response = array();

    if (isset($name)) {

        if ($name == 'clients') {
            $response = (new \Billing\Clients())->all($parameter);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'business') {
            $response = (new \Billing\Business())->all($parameter);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'services') {

            $response = (new \Billing\Services())->all($parameter ?? NULL, $_GET);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'invoices') {

            $response = (new \Billing\Invoices())->all($parameter ?? NULL, $_GET);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'transactions') {

            $response = (new \Billing\Transactions())->all($parameter ?? NULL, $_GET);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'orders') {

            $response = (new \Billing\Orders())->all($parameter ?? NULL, $_GET);

            if ((count($response) == 0) and (isset($parameter))) {
                $error['code'] = 404;
                $response['error'] = $error;
            }

        } elseif ($name == 'writeoff') {

            $response = (new \Billing\Orders())->writeOff();

            /*

            if (count($response) == 0) {
                $error['code'] = 404;
                $response['error'] = $error;
            }
                */
            
        } elseif ($name == 'invoices_update') {

            $response = (new \Billing\Invoices())->invoices_update();

            if (count($response) == 0) {
                $error['code'] = 404;
                $response['error'] = $error;
            }
            
        } elseif ($name == 'order_return') {

            if (is_null($parameter)) {
                $error['code'] = 404;
                $response['error'] = $error;
            } else {
                $response = (new \Billing\Orders())->return($parameter);
                if (count($response) == 0) {
                    $error['code'] = 404;
                    $response['error'] = $error;
                }
            }

        } else {
            $error['code'] = 405;
            $error['message'] = 'Запрос статуса с заданными параметрами не поддерживается';
            $response['error'] = $error;
        }
  
    } else {
        $error['code'] = 405;
        $error['message'] = 'Метод не установлен';
        $response['error'] = $error;
    }

    $return = $response;
  
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $response = array();
    $raw_post_data = file_get_contents('php://input');
    json_decode($raw_post_data);
    if (json_last_error() == JSON_ERROR_NONE) {
        $data = json_decode($raw_post_data, true);
        if (isset($name)) {

            if ($name == 'clients') {   
                $response = (new \Billing\Clients())->add($data);
            } elseif ($name == 'business') {   
                $response = (new \Billing\Business())->add($data);
            } elseif ($name == 'services') {
                $response = (new \Billing\Services())->add($data);
            } elseif ($name == 'invoices') {
                $response = (new \Billing\Invoices())->add($data);
            } elseif ($name == 'webhooks') {
                $response = (new \Billing\Acquiring())->insert($data);
                header("HTTP/1.1 200 OK");
                header("Content-type: text/plain");
                echo "OK";
                die();
            } else {
                $error['code'] = 405;
                $error['message'] = 'Запрос статуса с заданными параметрами не поддерживается';
                $response['error'] = $error;
            }

        } else {
            $error['code'] = 405;
            $error['message'] = 'Метод не установлен';
            $response['error'] = $error;
        }

         $return = $response;


    } else {
        $error['code'] = 415;
        $error['message'] = 'Запрос не в формате JSON';
        $return['error'] = $error;
    }

} elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    $raw_post_data = file_get_contents('php://input');
    json_decode($raw_post_data);
    if (json_last_error() == JSON_ERROR_NONE) {

        $data = json_decode($raw_post_data, true);
        if (isset($name)) {

            if ($name == 'clients') {
                $response = (new \Billing\Clients())->update($parameter ?? NULL, $data);
            } elseif ($name == 'business') {
                $response = (new \Billing\Business())->update($parameter ?? NULL, $data);
            } elseif ($name == 'services') {
                $response = (new \Billing\Services())->update($parameter ?? NULL, $data);
            } elseif ($name == 'invoices') {
                $response = (new \Billing\Invoices())->update($parameter ?? NULL, $data);
            } else {
                $error['code'] = 405;
                $error['message'] = 'Запрос статуса с заданными параметрами не поддерживается';
                $response['error'] = $error; 
            }

        } else {
            $error['code'] = 405;
            $error['message'] = 'Метод не установлен';
        }
    
        $return = $response;
    
    } else {
        $error['code'] = 415;
        $error['message'] = 'Запрос не в формате JSON';
        $return['error'] = $error;
    }
    
}

if (isset($return['error']['code'])) {
    $code = $return['error']['code'];
    if ($code == 404) {
        header("HTTP/1.1 404 Not Found");
        die();
    } elseif ($code == 204) {
        header("HTTP/1.0 204 No Response");
    }
    
    $return['message'] = $return['error']['message'];
    $return['error'] = true;
}

echo json_encode($return, JSON_UNESCAPED_UNICODE, JSON_PRETTY_PRINT);
exit();