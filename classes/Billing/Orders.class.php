<?php

namespace Billing;

/* Заказанные услуги и товары клиентом */

class Orders
{

    /* Constructor */

	const DATE_FROM_ONE_TIME_ACT = "2024-05-30";
	
	public function __construct()
	{
        $this->db = new \Billing\DB(); // подключение к БД
    }
	
	/* Получение списка заказов */
	public function all(
        $id = NULL, // ID заказа
        $data = NULL // допольнительные параметры, фильтр
    ) 
    {

        if ($id != NULL) {
            $select = $this->db->query('SELECT * FROM orders WHERE id = ?', $id);
        } 
        else {

            $sql_vars = array();
		
            $sql = 'SELECT * FROM orders WHERE ';
			
			/* Собираем поля, по которым надо искать */

			if (array_key_exists('inn', $data))
			{
				$sql .= 'inn = ? AND ';
				$sql_vars[] = $data['inn'];
            }
            
            if (array_key_exists('date_start', $data))
			{
				$sql .= 'date_start <= ? AND ';
				$sql_vars[] = $data['date_start'];
			}
			
			if (array_key_exists('status', $data))
			{
				$sql .= 'status = ? AND ';
				$sql_vars[] = $data['status'];
            }

			if (count($sql_vars) > 0) {
				$sql = mb_substr($sql, 0, -5);
				$sql .= ' ORDER BY id DESC';
				$select = $this->db->query($sql, $sql_vars);
			} else {
				$sql = mb_substr($sql, 0, -7);
				$sql .= ' ORDER BY id DESC';
				$select = $this->db->query($sql);
			}
            
        }

        $orders = $select->fetchAll();

        if ($id === NULL)
            return $orders;
        else {
            if (count($orders) == 1) return $orders[0];
            else return $orders;
        }
		
    }

    /* Добавление нового заказа */
	public function add($inn, $service_id, $invoice_id, $date_start, $period, $amount) 
	{
  
        if ($period == 0) $date_end = $date_start;
        else {
            $date_end = date("Y-m-d", strtotime("+".$period." month", strtotime($date_start)));
        }

        $sql_vars = array(
            $inn,
			$service_id,
			$invoice_id,
            $date_start,
            $date_end,
			$period,
			$amount
        );

		$insert = $this->db->query('INSERT INTO orders (inn, service_id, invoice_id, date_start, date_end, period, amount) VALUES (?,?,?,?,?,?,?)', $sql_vars);
		
    }

    /* Изменение статуса заказа */
	public function update($id, $data)
	{  
		
		$sql_vars = array();
		
		$sql = 'UPDATE orders SET ';
		
		/* Собираем поля, которые надо обновить */

		if (array_key_exists('status', $data))
		{
		  $sql .= 'status = ?, ';
		  $sql_vars[] = $data['status'];
		}

		if (array_key_exists('date_end', $data))
		{
		  $sql .= 'date_end = ?, ';
		  $sql_vars[] = $data['date_end'];
		}
		
		if (count($sql_vars) == 0) {
			$return['error'] = true;
			$return['message'] = 'Не указаны данные для изменения';
			return $return;
		}

		$sql = mb_substr($sql, 0, -2) . ' WHERE (id = ?)';
		$sql_vars[] = $id;

		/* Отправляем запрос в БД на изменение */
		$update = $this->db->query($sql, $sql_vars);

	}

	/* Списание с баланса за услуги и товары */
	public function writeOff($inn = false, $onetime = false)
	{

		// Получаем все заказы, которые еще не проведены (не списаны с баланса клиента)

		if (($onetime) and ($inn)) {
			// для разовых сразу
			$orders = $this->all(NULL, array('status'=>0, 'inn' => $inn));
		} else {
			// для остальных только с начала отетного периода
			$orders = $this->all(NULL, array('status'=>0, 'date_start' => date('Y-m-d H:i:s')));
		}

		// задаем массив клиентов, для которых необходимо сформировать акты
		$clients = array();

		$amount = 0;

		foreach ($orders as $order) {

			//Получаем информацию об услуге
			$service_info = (new \Billing\Services())->all($order['service_id']);

			if ($order['period'] == 0) {
				
				//Изменяем внутренний баланс клиента
				$balance_update = (new \Billing\Clients())
                ->changeBalance(
                    $order['inn'],
					$order['service_id'],
					$order['id'],
                    0 - ($service_info['cost'] * $order['amount']),
					$service_info['title'],
					$order['date_start']
				);

				//Закрываем услугу (полностью проведен)
				$this->update($order['id'], array('status' => 1));

				$amount++;
				
			} else {

				if ((!$onetime) and (!$inn)) {

					$period = $order['period'];
					$start_date = $order['date_start'];
					$next_date = $start_date;
					$prev_date = $start_date;


					if (strtotime($order['date_start']) >= strtotime(self::DATE_FROM_ONE_TIME_ACT)) {

						////////////////////////////////
						/// Акт сразу на закрытие услуги
						////////////////////////////////
					
						$prev_date = $next_date;
						$next_date = date("Y-m-d H:i:s", strtotime("+".$period." month", strtotime($start_date)));

		
						$txns = (new \Billing\Transactions())
							->all(NULL, 
								array(
									'inn' => $order['inn'],
									'source' => $order['service_id'],
									'order_id' => $order['id'],
									'date_start' => $prev_date,
									'date_end' => $next_date
								)
							);

						
						
						if (count($txns) == 0) {

							// добавляем информацию об услуге, на которую нужно сформировать акт

							if (!array_key_exists($order['inn'], $clients))
							$clients[$order['inn']] = array();

							if (!array_key_exists($order['invoice_id'], $clients[$order['inn']]))
								$clients[$order['inn']][$order['invoice_id']] = array();

							$order_invoice = array(
								'id' => $order['service_id'],
								'amount' => $order['amount'],
								'date_start' => $order['date_start'],
								'onetime' => true
							);

							$clients[$order['inn']][$order['invoice_id']][] = $order_invoice;

							////

							//Изменяем внутренний баланс клиента
						
							$balance_update = (new \Billing\Clients())
							->changeBalance(
								$order['inn'],
								$order['service_id'],
								$order['id'],
								0 - ($service_info['cost'] * $order['amount']),
								$service_info['title'],
								$prev_date
							);

							//Закрываем услугу
							$this->update($order['id'], array('status' => 1));

							$amount++;

						}

						////////////////////////////////
						////////////////////////////////
						////////////////////////////////

					} else {


						////////////////////////////////
						/// Акт на каждый месяц
						////////////////////////////////

				
						$flag = false;
						$p = 0;

						while (!$flag) {


							$p++;
							$prev_date = $next_date;
							$next_date = date("Y-m-d H:i:s", strtotime("+".$p." month", strtotime($start_date)));

							$txns = (new \Billing\Transactions())
								->all(NULL, 
									array(
										'inn' => $order['inn'],
										'source' => $order['service_id'],
										'order_id' => $order['id'],
										'date_start' => $prev_date,
										'date_end' => $next_date
									)
								);

								
							if (count($txns) == 0) {

								// добавляем информацию об услуге, на которую нужно сформировать ежемесячный акт

								if (!array_key_exists($order['inn'], $clients))
								$clients[$order['inn']] = array();

								if (!array_key_exists($order['invoice_id'], $clients[$order['inn']]))
									$clients[$order['inn']][$order['invoice_id']] = array();

								$order_invoice = array(
									'id' => $order['service_id'],
									'amount' => $order['amount'],
									'date_start' => $order['date_start'],
									'onetime' => true
								);

								$clients[$order['inn']][$order['invoice_id']][] = $order_invoice;

								////

								//Изменяем внутренний баланс клиента
								$balance_update = (new \Billing\Clients())
								->changeBalance(
									$order['inn'],
									$order['service_id'],
									$order['id'],
									0 - ($service_info['cost'] * $order['amount'])/$period,
									$service_info['title'],
									$prev_date
								);

								//Закрываем услугу (полностью проведен)
								if (strtotime($order['date_end']) < strtotime('now')) {
									$this->update($order['id'], array('status' => 1));
								}

								$amount++;

							}


							if ( (strtotime($next_date) > strtotime('now')) or ($p >= $period) ) {
								$flag = true;
								break;
							}
							
						}
					}
					

					////////////////////////////////
					////////////////////////////////
					////////////////////////////////
					
					//Получаем список транзакции
					$txns = (new \Billing\Transactions())
						->all(NULL, 
							array(
								'inn' => $order['inn'],
								'date_start'=>date('Y-m-d H:i:s'),
								'date_start'=>date('Y-m-d H:i:s')
							)
						);

				}

			}
			

		}

		

		if (count($clients) > 0) {

			foreach ($clients as $inn=>$invoices) {
				foreach ($invoices as $uid=>$services_now) {
				
					$invoice = (new \Billing\Invoices())
						->all(null, 
							array(
								'uid'=>$uid
							)
						);

					$invoice = reset($invoice);

					if (!array_key_exists('acts', $invoice['documents']))
						$invoice['documents']['acts'] = array();

					if (
						(count($invoice['documents']['acts']) == 0 && strtotime($invoice["created"]) > strtotime(self::DATE_FROM_ONE_TIME_ACT))
						||
						(count($invoice['documents']['acts']) != 0 && strtotime($invoice["created"]) <= strtotime(self::DATE_FROM_ONE_TIME_ACT))
						)
					{

						$acts = (new \Billing\Invoices())
							->genDocument(
								$uid,
								$inn,
								$services_now,
								'act'
							);
						
						foreach ($acts as $act) {
							$invoice['documents']['acts'][] = $act;
						}


						$sql_vars = array();
			
						$sql = 'UPDATE invoices SET ';

						$sql .= 'documents = ?, ';
						$sql_vars[] = json_encode($invoice['documents'], JSON_UNESCAPED_UNICODE);

						$sql = mb_substr($sql, 0, -2) . ' WHERE (id = ?)';
						$sql_vars[] = $invoice['id'];

						/* Отправляем запрос в БД на изменение */
						$update = $this->db->query($sql, $sql_vars);

					}

				
				}
			}

		}
		
		

		return array(
			"success" => true,
			"message" => "Списания успешно выполнены. Операций: ".$amount.""
		);


	}

	/* Отмена периодической услуги, возврат остатка средств на баланс */
	public function return($order_id)
	{
		if (is_null($order_id)) {
			return array(
				"error" => true,
				"message" => "ID заказа не указан"
			);
		}

		$order = $this->all($order_id);

		if ((count($order) == 0) or ($order['status'] == 2)) {

			return array();

		} else {

			//Получаем информацию об услуге
			$service_info = (new \Billing\Services())->all($order['service_id']);

			if ($order['period'] == 0) {

				//Считаем сумму для возврата баланса
				$return_cash = $service_info['cost'] * $order['amount'];

			} else {

				$start_date = $order['date_start'];
				$next_date = $start_date;
				$prev_date = $start_date;

				$flag = false;
				$p = 0;

				while (!$flag) {

					$p++;
					$prev_date = $next_date;
					$next_date = date("Y-m-d H:i:s", strtotime("+".$p." month", strtotime($start_date)));


					if (strtotime($next_date) > strtotime('now')) {
						$flag = true;
						break;
					}
				}

				//Считаем кол-во дней пользования услугой в текущем месяце
				$days_diff = ceil((strtotime(date('Y-m-d')) - strtotime($prev_date))/(60*60*24));

				//Считаем сумму для возврата баланса
				$return_cash = ceil(($service_info['cost'] * $order['amount']/$order['period'])*(1 - $days_diff/30));

			}

			//Изменяем внутренний баланс клиента
			$balance_update = (new \Billing\Clients())
			->changeBalance(
				$order['inn'],
				$order['service_id'],
				$order['id'],
				$return_cash,
				'Отмена и возврат остатка: '.$service_info['title'],
				date('Y-m-d H:i:s')
			);

			//Закрываем услугу
			$this->update($order['id'], array('status' => 2, 'date_end' => date('Y-m-d H:i:s')));

			return $this->all($order['id']);

		}


	}



}
