<?php

namespace Billing;

/* Транзакции */

class Transactions
{

	public function __construct()
	{
        $this->db = new \Billing\DB();
    }
	
	/* Получение списка транзакций */
	public function all(
        $id = NULL, // ID услуги
        $data = NULL // допольнительные параметры, фильтр
    ) 
    {
        
        $transactions = array();

        if ($id != NULL) {
            $select = $this->db->query('SELECT * FROM transactions WHERE id = ?', $id);
        } 
        else {

            $sql_vars = array();
		
            $sql = 'SELECT * FROM transactions WHERE ';
			
			/* Собираем поля, по которым надо искать */

			if (array_key_exists('inn', $data))
			{
				$sql .= 'inn = ? AND ';
				$sql_vars[] = $data['inn'];
            }

            if (array_key_exists('source', $data))
			{
				$sql .= 'source = ? AND ';
				$sql_vars[] = $data['source'];
            }

            if (array_key_exists('order_id', $data))
			{
				$sql .= 'order_id = ? AND ';
				$sql_vars[] = $data['order_id'];
            }
            
            if (array_key_exists('date_start', $data))
			{
				$sql .= 'date_start >= ? AND ';
				$sql_vars[] = $data['date_start'];
            }
            
            if (array_key_exists('date_end', $data))
			{
				$sql .= 'date_start <= ? AND ';
				$sql_vars[] = $data['date_end'];
            }
            

			if (count($sql_vars) > 0) {
				$sql = mb_substr($sql, 0, -5);
			} else {
				$sql = mb_substr($sql, 0, -7);
			}

            $sql .= ' ORDER BY id DESC';
            
            $select = $this->db->query($sql, $sql_vars);
        }

        $transactions = $select->fetchAll();

        if ($id === NULL)
            return $transactions;
        else {
            if (count($transactions) == 1) return $transactions[0];
            else return $transactions;
        }
		
    }

    /* Добавление новой транзакции */
	public function add($inn, $source, $order_id, $title, $value, $total, $date_start) 
	{

        $sql_vars = array(
            $inn,
            $source,
            $order_id,
            $title,
            $value,
            $total,
            $date_start
        );

		$insert = $this->db->query('INSERT INTO transactions (inn, source, order_id, title, value, total, date_start) VALUES (?,?,?,?,?,?,?)', $sql_vars);
		
    }

}
